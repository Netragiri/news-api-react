import React from 'react'
import Home from './Components/Home'
import { Routes,Route } from 'react-router-dom'
import DetailedData from './Components/DetailedData'


function App() {
  return (
    <>
      <Routes>
          <Route path='/' element={<Home />}></Route>
          <Route path='detaildata' element={<DetailedData />}></Route>
      </Routes>
    </>
  )
}

export default App
