import React from 'react'
import { useLocation } from 'react-router-dom'

function DetailedData() {
 const location=useLocation()
  return (
    <div>
      <h1>{location.state.title}</h1>
      <p><strong>Author name:</strong>{location.state.author}</p>
      <p><strong>Description : </strong>{location.state.description}</p>
      <p><strong>Publish date:</strong>{location.state.publishdate}</p>
      <img style={{height:'500px',width:'500px'}} src={location.state.image}></img>
    </div>
  )
}

export default DetailedData
