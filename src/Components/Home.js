
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function Home() {

  const nav=useNavigate()
  const[article,setArticle]=useState('')
  const[data,setData]=useState('')
  const[answer,setAnswer]=useState([])
  const[filteredarray,setFilteredarray]=useState([])
  const[slice,setSlice]=useState(5)




  //for the load article from api
  const submithandler=()=>{
    fetch(`https://newsapi.org/v2/everything?q=${article}&apiKey=d3a68d3a93a54948a016a1553bc4d20c`)
    .then(response=>response.json())
    .then(data=>{
      setAnswer(data.articles)
      setFilteredarray(data.articles)
      console.log(data.articles)
      setSlice(5)
    }
    )}



    //for search data from given article
    //answer is array of articles
    
    const searchhandler=(e)=>{
      setData(e.target.value)


      //it searches in both title and description the text we serch
      const filteredarray=answer.filter((item)=>
         item.title.toLowerCase().includes(data.toLowerCase()) || item.description.toLowerCase().includes(data.toLowerCase())
      )
      console.log(filteredarray)
      setFilteredarray(filteredarray)
    }
    


    //for loadmore data button
    const clickhandler=()=>{
      setSlice(prevalue=>prevalue+5)
    }
  return (
    <div className="App container">
      <h3 className='my-5'>Search for article</h3>

      {/* search the topic of article */}
      <input type='text' className='form-control' onChange={(e)=>{setArticle(e.target.value)}} value={article}></input>
      <button className='btn btn-info my-5' onClick={submithandler}>Search Article</button><br />

      {/* search the data u want to see when the */}
      {answer.length ==0 ?null : <><h5>Search data in articles</h5><input className='form-control' type='text' value={data} onChange={searchhandler} ></input></>}
     
      
      {/* <button onClick={searchhandler}>Search for data</button> */}
      




      <table border='1' className='table table-dark my-5'>
        <thead>
        <tr >
          <th scope='col' >title:</th>
          <th>description</th>
        </tr>
        </thead>
        
        <tbody>
          {filteredarray.slice(0,slice).map((item,key)=>{
        return (
          <tr key={key} >


            <td>{item.title}</td>
            <td>{item.description}  <button className='btn btn-link' onClick={()=>{nav('detaildata',
          {state:
            {title:item.title,
            author:item.author,
            description:item.description,
            publishdate:item.publishedAt,
            image:item.urlToImage
            }})}} >more details</button></td>
           
          </tr>
        )
      })}
        
        </tbody>
      </table>
      
{/* this will check if length is zero load more button is disable and all record shows then also load more button will disapper */}
     { answer.length===0 || answer.length===slice ? null :<button className='btn' onClick={clickhandler}>Load more</button>}
      
    </div>
  );
}

export default Home
